# -*- coding: utf-8 -*-
# Generated by Django 1.11.25 on 2019-11-16 02:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0004_auto_20191115_2208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assets',
            name='B_time',
            field=models.DateTimeField(auto_now_add=True, verbose_name='购买时间'),
        ),
        migrations.AlterField(
            model_name='assets',
            name='buyer',
            field=models.CharField(max_length=32, verbose_name='购买者'),
        ),
        migrations.AlterField(
            model_name='assets',
            name='description',
            field=models.CharField(max_length=255, verbose_name='描述'),
        ),
        migrations.AlterField(
            model_name='assets',
            name='laboratory',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Ass', to='app01.Laboratory', verbose_name='资产归属'),
        ),
        migrations.AlterField(
            model_name='assets',
            name='name',
            field=models.CharField(max_length=100, verbose_name='资产名'),
        ),
        migrations.AlterField(
            model_name='assets',
            name='number',
            field=models.IntegerField(verbose_name='数量'),
        ),
        migrations.AlterField(
            model_name='laboratory',
            name='floor',
            field=models.IntegerField(verbose_name='楼层'),
        ),
        migrations.AlterField(
            model_name='laboratory',
            name='head',
            field=models.CharField(max_length=32, verbose_name='负责人'),
        ),
        migrations.AlterField(
            model_name='laboratory',
            name='name',
            field=models.CharField(max_length=32, verbose_name='实验室名称'),
        ),
        migrations.AlterField(
            model_name='laboratory',
            name='room',
            field=models.IntegerField(verbose_name='房间号'),
        ),
        migrations.AlterField(
            model_name='laboratory',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Lab', to='app01.User', verbose_name='管理员'),
        ),
        migrations.AlterField(
            model_name='user',
            name='password',
            field=models.CharField(max_length=32, verbose_name='密码'),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.IntegerField(verbose_name='电话号'),
        ),
        migrations.AlterField(
            model_name='user',
            name='user',
            field=models.CharField(default='小绿', max_length=32, verbose_name='昵称'),
        ),
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(max_length=32, verbose_name='用户名'),
        ),
    ]
