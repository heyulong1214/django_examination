# -*- coding: utf-8 -*-
# Generated by Django 1.11.25 on 2019-11-15 08:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0002_auto_20191115_1600'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='user',
            field=models.CharField(default='小绿', max_length=32),
        ),
    ]
