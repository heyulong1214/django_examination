from django.shortcuts import render,redirect,HttpResponse,reverse
from django import forms
from app01 import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.forms import models as form_model


def assets(request):
    all_assets=models.Assets.objects.all()
    return render(request, 'assets.html', {'all_assets':all_assets})


def laboratory(request):
    all_laboratory = models.Laboratory.objects.all()
    return render(request,'laboratory.html',{'all_laboratory':all_laboratory})


def user(request):
    all_user=models.User.objects.all()
    return render(request, 'user.html', {'all_user':all_user})


class User(forms.Form):
    def clean(self):
        user = self.cleaned_data.get('user')
        if models.User.objects.filter(user=user):
            raise ValidationError('用户名重复，请重新选择输入')
        return user

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user=self.cleaned_data.get('user')
        if username != user:
            return self.cleaned_data
        else:
            self.add_error('username','昵称不能与用户名相同')
            # raise ValidationError('用户名与昵称相同')



    user = forms.CharField(
        max_length=32,
        label='用户名',
        error_messages={
            'required':'用户名与别人的重复了，垃圾',
            'max_length':'输的太多了'
        })

    username = forms.CharField(
        max_length=32,
        label='姓名',
        error_messages={
            'required': '用户名与别人的重复了，垃圾',
            'max_length': '输的太多了'
        })
    password = forms.CharField(
        label='密码',
        min_length=4,
        widget=forms.PasswordInput,
        error_messages={
            'required': '错了'
        })

    phone = forms.CharField(
        min_length=11,
        label='电话',
        max_length=11,
        validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')]
    )


from django.views.decorators.csrf import ensure_csrf_cookie
@ensure_csrf_cookie
def user_add(request):
    user = User()
    error = ''
    if request.method == 'POST':
        # 获取用户提交的信息，校验之后存入数据库中，然后跳转到展示页面
        user=User(request.POST)
        user_name = request.POST.get('user')
        username = request.POST.get('username')
        password = request.POST.get('password')
        phone = request.POST.get('phone')
        if user.is_valid():
            models.User.objects.create(user=user_name,username=username,password=password,phone=phone)
            return redirect(reverse('user'))
    return render(request,'user_add.html',{'user':user,'error':error})


class Assets(forms.Form):
    name = forms.CharField(label='物资')
    description=forms.CharField(label='描述')
    number = forms.IntegerField(label='购买数量')
    # b_time=forms.DateTimeField()
    # buyer=forms.CharField(label='购买人')
    buyer=forms.ChoiceField(label='购买人',
                            choices=models.User.objects.values_list('id','user'))
    laboratory_id = forms.ChoiceField(
        label='所属实验室',
        choices=models.Laboratory.objects.values_list('id','name'))

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if models.Assets.objects.filter(name=name):
            raise ValidationError('用户名重复，请重新选择输入')
        return name


@ensure_csrf_cookie
def assets_add(request):
    assets=Assets()
    if request.method == 'POST':
        assets = Assets(request.POST)
        name = request.POST.get('name')
        description=request.POST.get('description')
        number=request.POST.get('number')
        buyer = request.POST.get('buyer')
        laboratory_id=request.POST.get('laboratory_id')
        lab_asset=models.Laboratory.objects.filter(pk=laboratory_id).first()
        print(111111)
        if assets.is_valid():
            print(22222222)
            models.Assets.objects.create(name=name,description=description,number=number,buyer=buyer,laboratory=lab_asset)
            return redirect(reverse('assets'))
    return render(request,'assets_add.html',{'assets':assets})


class Laboratory(forms.Form):
    name = forms.CharField(label='实验室')
    floor = forms.IntegerField(label='楼层')
    room = forms.IntegerField(label='房间号')
    head = forms.ChoiceField(
        label='负责人',
        choices=models.User.objects.values_list('id','user')
    )
    user=forms.ChoiceField(
        label='管理员',
        choices=models.User.objects.values_list('id','username')
    )


    def clean_name(self):
        name = self.cleaned_data.get('name')
        if models.Laboratory.objects.filter(name=name):
            raise ValidationError('实验室已存在')
        return name


@ensure_csrf_cookie
def laboratory_add(request):
    laboratory=Laboratory()
    if request.method == "POST":
        laboratory=Laboratory(request.POST)
        name = request.POST.get('name')
        floor= request.POST.get('floor')
        room = request.POST.get('room')
        head = request.POST.get('head')
        user = request.POST.get('user')
        ret_head = models.User.objects.filter(pk=user).first()
        ret=models.User.objects.filter(pk=user).first()
        if laboratory.is_valid():
            models.Laboratory.objects.create(name=name,floor=floor,room=room,head=ret_head,user=ret)
            return redirect(reverse('laboratory'))

    return render(request,'laboratory_add.html',{'laboratory':laboratory})


def assets_edit(request):
    error = ''
    assets_id = request.GET.get('pk')
    assets_obj = models.Assets.objects.filter(pk=assets_id).first()
    all_lab = models.Laboratory.objects.all()
    print(assets_obj)
    # assets_obj = models.Assets.objects.filter(pk=pk).first()
    if request.method == "POST":
        assets_name = request.POST.get('assets_name')
        assets_description = request.POST.get('assets_description')
        assets_number = request.POST.get('assets_number')
        assets_btime = request.POST.get('assets_obj.btime')
        assets_buyer = request.POST.get('assets_buyer')
        assets_lab = request.POST.get('assets_lab')
        lab = models.Laboratory.objects.filter(pk=assets_lab).first()
        print(lab)
        print(111111)
        if not assets_name:
            error='资产名不能为空'
        else:
            assets_obj.name = assets_name
            # print(2222222)
            assets_obj.description = assets_description
            assets_obj.number = assets_number
            assets_obj.btime = assets_btime
            assets_obj.buyer = assets_buyer
            assets_obj.laboratory = lab
            assets_obj.save()
            print(22222222)
            return redirect(reverse('assets'))
    all_assets = models.Assets.objects.all()
    return render(request,'assets_edit.html',{'assets_obj':assets_obj,'error':error,'all_assets':all_assets,'all_lab':all_lab})


def user_edit(request):
    error = ''
    user_id = request.GET.get('pk')
    user_obj = models.User.objects.filter(pk=user_id).first()
    print(user_obj)
    if request.method == "POST":
        user_name = request.POST.get('user_name')
        user_password = request.POST.get('user_password')
        user_user = request.POST.get('user_user')
        user_phone = request.POST.get('user_phone')
        if not user_user:
            error = '昵称不能为空'
        else:
            user_obj.username = user_name
            user_obj.password = user_password
            user_obj.user  = user_user
            user_obj.phone = user_phone
            user_obj.save()
            return redirect(reverse('user'))
    all_user = models.User.objects.all()
    return render(request, 'user_edit.html', {'user_obj': user_obj, 'error': error, 'all_user': all_user})


def laboratory_edit(request):
    error = ''
    laboratory_id = request.GET.get('pk')
    lab_obj = models.Laboratory.objects.filter(pk=laboratory_id).first()
    print(lab_obj)
    if request.method == "POST":
        lab_name  = request.POST.get('lab_name')
        lab_floor = request.POST.get('lab_floor')
        lab_room = request.POST.get('lab_room')
        lab_head = request.POST.get('lab_head')
        lab_user = request.POST.get('lab_user')
        # username = models.User.objects.filter(pk=lab_head).first()
        user_lab = models.User.objects.filter(pk=lab_user).first()
        if not lab_name:
            error = '实验室名称不能为空'
        else:
            lab_obj.name = lab_name
            lab_obj.floor = lab_floor
            lab_obj.room  = lab_room
            lab_obj.head = lab_head
            lab_obj.user = user_lab
            lab_obj.save()
            return redirect(reverse('laboratory'))
    all_lab = models.Laboratory.objects.all()
    all_user= models.User.objects.all()
    return render(request, 'laboratory_edit.html', {'lab_obj': lab_obj, 'error': error, 'all_lab': all_lab,'all_user':all_user})


def laboratory_del(request):
    id = request.GET.get('id')
    print(id)
    models.Laboratory.objects.filter(id=id).delete()
    return redirect(reverse('laboratory'))


def assets_del(request):
    id = request.GET.get('id')
    print(id)
    models.Assets.objects.filter(id=id).delete()
    return redirect(reverse('assets'))


def user_del(request):
    id = request.GET.get('id')
    print(id)
    models.User.objects.filter(id=id).delete()
    return redirect(reverse('user'))