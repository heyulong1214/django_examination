"""考试 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from app01 import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^assets/', views.assets,name='assets'),
    url(r'^laboratory/', views.laboratory,name='laboratory'),
    url(r'^user/', views.user,name='user'),
    url(r'^user_add/', views.user_add,name='user_add'),
    url(r'^assets_edit/', views.assets_edit,name='assets_edit'),
    url(r'^assets_add/', views.assets_add,name='assets_add'),
    url(r'^laboratory_add/', views.laboratory_add,name='laboratory_add'),
    url(r'^user_edit/', views.user_edit,name='user_edit'),
    url(r'^laboratory_edit/', views.laboratory_edit,name='laboratory_edit'),
    url(r'^laboratory_del/', views.laboratory_del,name='laboratory_del'),
    url(r'^user_del/', views.user_del,name='user_del'),
    url(r'^assets_del/', views.assets_del,name='assets_del'),
]
