from django.db import models


# Create your models here.


class User(models.Model):
    '''
    用户表
    '''
    username = models.CharField(max_length=32,verbose_name='用户名')
    password = models.CharField(max_length=32,verbose_name='密码')
    user = models.CharField(max_length=32, default='小绿',verbose_name='昵称')
    phone = models.IntegerField(verbose_name='电话号')



class Laboratory(models.Model):
    '''
    实验室表
    '''
    name = models.CharField(max_length=32,verbose_name='实验室名称')
    floor = models.IntegerField(verbose_name='楼层')
    room = models.IntegerField(verbose_name='房间号')
    head = models.CharField(max_length=32,verbose_name='负责人')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Lab', null=True, blank=True,verbose_name='管理员')



class Assets(models.Model):
    '''
    资产表
    '''
    name = models.CharField(max_length=100,verbose_name='资产名')
    description = models.CharField(max_length=255,verbose_name='描述')
    number = models.IntegerField(verbose_name='数量')
    B_time = models.DateTimeField(auto_now_add=True,verbose_name='购买时间',null=True)
    buyer = models.CharField(max_length=32,verbose_name='购买者')
    laboratory = models.ForeignKey(Laboratory, on_delete=models.CASCADE, related_name='Ass',verbose_name='资产归属')
